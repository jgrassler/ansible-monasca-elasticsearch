# ansible-elasticsearch

# Avoiding split-brain
Role leverages ```elasticsearch_cluster_nodes``` variable to determine:
```discovery.zen.minimum_master_nodes``` according to following equation:
```yml
minimum_master_nodes: {{ ((elasticsearch_cluster_nodes|length/2)+1)|int }}
```
In other words, assuming 4 nodes in cluster, **minimum_master_nodes** would
be equal to **3**.

# Variables

## Optional
* **download_timeout** - defines a time for timeout when downloading ELK tar files, defaults to 600
* **download_tmp_dir** - location where logstash is being download (default /tmp)
* **elasticsearch_data_dir** - path to location where ES should store data in. It may be defined
as a string or an array. With string it is also possible to pass comma-delimited list of
directories.
* **elasticsearch_repo_dir** - path to Shared File System Repository for backup data.
It may be defined as a string or an array. With string it also possible to pass comma-delimited
list of directories.
* **elasticsearch_log_dir** - path to location where ES should write log
* **elasticsearch_port** - port under which ES should be available, default to **9200**
* **elasticsearch_publish_port** - port used to node-to-node communication, default to **9300**
* **elasticsearch_host** - default to ```127.0.0.1```. Sets both publish and bind host
* **elasticsearch_bind_host** - default to ```elasticsearch_host```
* **elasticsearch_publish_host** - default to ```elasticsearch_host```
* **elasticsearch_url** - default to
```yml
elasticsearch_url: "http://{{ elasticsearch_publish_host }}:{{ elasticsearch_port }}"
```
* **elastic_install_user_group** - should install user (```elastic_user```) and group (```elastic_group```), defaults to ```True```

## Process and resources control (user,group)

Following variables are used to narrow down ElasticSearch access only to those
resources which it actually needs.

* **elastic_user** - user name for ES resources
* **elastic_group** - group name for ES resources

## Process and resources control (limits)
* **elasticsearch_max_open_files_soft_limit** - default to ```16384```. Set the soft limit of max open files for "elastic_user".
* **elasticsearch_max_open_files_hard_limit** - default to ```65536```. Set the hard limit of max open files for "elastic_user".
* **elasticsearch_max_procs** - default to ```65536```. Set the limit of max processes for "elastic_user".
* **elasticsearch_max_locked_memory** - default to ```infinity```. Set the maximum number of bytes of memory that may be locked into RAM.
* **elasticsearch_vm_max_map_count** - default to ```262144```. Set max virtual memory areas.
* **elasticsearch_memory_lock:** - default to ```true```. Lock the process address space into RAM.
* **elasticsearch_heap_size** - default to ```10g```. Set the min (Xms) and max (Xmx) sizes.

## Node properties
* **elasticsearch_node_name** - pass arbitrary node name, by default hostname
where ElasticSearch instance is being launched is used
* **elasticsearch_is_master_node** - by default it is true (same as default ES configuration), you can modify this
value if needed
* **elasticsearch_is_data_node** - by default it is true (same as default ES configuration), you can modify this
value if needed

## Cluster settings
* **elasticsearch_cluster_name** - name of the cluster that nodes join to
* **elasticsearch_cluster_nodes** - an array of IP addresses of all nodes within cluster. Please note that if this variable is empty (which is default value) following property
```discovery.zen.ping.multicast.enabled``` is set to false.
That's because an arbitrary list of hosts has been specified
to be members of the cluster. Otherwise
```discovery.zen.ping.multicast.enabled``` will be set to true.

# Additional information
This role adds two attributes to the given ElasticSearch node. Those are:
* ```node.installation_method``` - equal to *ansible*, describes installation method
* ```node.installation_ts``` - equal to current time (i.e the moment of installation)lookup('pipe','date')lookup('pipe','date')lookup('pipe','date')

## License

Apache License, Version 2.0

## Author Information

Tomasz Trebski
